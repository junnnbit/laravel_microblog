<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\UsersController;
use App\Http\Controllers\PostsController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/signup', [UsersController::class, 'signup']);
Route::post('/login', [UsersController::class, 'login']);
Route::middleware('auth:api')->get('/users', [UsersController::class, 'index']);
Route::middleware('auth:api')->post('/logout', [UsersController::class, 'logout']);
Route::middleware('auth:api')->post('/home', [PostsController::class, 'index']);
