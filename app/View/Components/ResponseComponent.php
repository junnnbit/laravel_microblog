<?php

namespace App\View\Components;

use Illuminate\View\Component;

class ResponseComponent extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.response-component');
    }

    public function returnResponseDetails($status_code = '', $messageId = '', $message = '', $data = []) {
        $body = [
            'status_code' => $status_code,
            'message_id' => __($messageId), 
            'message' => __($message)
        ];

        if ($data) {
            $body['data'] = $data;
        }

        return $body;
    }
}
