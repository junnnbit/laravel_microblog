<?php

namespace App\View\Components;

use Illuminate\View\Component;

class ValidationComponent extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.validation-component');
    }

    public function validationErrorWithMessageAndData($data = [], $message = '', $messageId = '') {
        $body = [
            'status_code' => 400,
            'message_id' => __('INVALID_PARAMETERS'), 
            'message' => __('Invalid Parameters.')
        ];

        if ($data) {
            $body['data'] = $data;
        }

        if ($message) {
            $body['message'] = __($message);
        }

        if ($messageId) {
            $body['message_id'] = __($messageId);
        }

        // $this->code = 400;

        // return $this->responseBody($body);
        return $body;

    }

    // public function apiKeyValidator() {
    //     $token = bcrypt($request->header('API_KEY'));
    //     $registeredToken = bcrypt('Laravel8MicroblogApiAccessKey');

    //     if ($token !== $registeredToken) {
    //         return
    //     }
    // }
}
