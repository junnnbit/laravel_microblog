<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Models\Posts;
use App\Models\Comments;
use App\Models\FollowersFollowing;
use App\Models\Images;
use App\Models\LikedPosts;
use App\Models\Users;

use App\View\Components\ResponseComponent;

class PostsController extends Controller
{
    public function __construct()
    {
        $this->responseComponent = new ResponseComponent();
    }

    public function index(Request $request){
        $userLoggedIn = $request->user();

        $users_id = $userLoggedIn['id'];

        //active user details
        $user = Users::where('id', $users_id)->first();

        if ($user) {
            if (!empty($user)) {
                //to follow users list
                $activeUserFollowing = FollowersFollowing::select(['following_id'])->where(['follower_id' => $users_id, 'is_deleted' => false])->get();
                $users = Users::where('id', '<>', $users_id)->whereNotIn('id', $activeUserFollowing)->get();

                //posts
                $postsQuery = Posts::where('users_id', $users_id)->get();
                $posts = collect($postsQuery)->map(function ($row) use ($users_id) {

                    $row['post'] = ($row['posts_id'] === null ? [] : Posts::where(['posts.id' => intval($row['posts_id'])])->first());
                        
                    if (!empty($row['post'])) {
                        $row['post']['user'] = Users::where(['id' => intval($row['post']['users_id'])])->first();
                        $row['post']['images'] = Images::where(['post_id' => intval($row['post']['id']), 'is_deleted' => false])->get();
                    }
                    $row['images'] = Images::where(['post_id' => intval($row['id']), 'is_deleted' => false])->get();
                    $row['user'] = Users::where(['id' => intval($row['users_id'])])->first();
                    $row['liked'] =  LikedPosts::select(['id', 'users_id', 'posts_id', 'is_deleted'])->where(['posts_id' => intval($row['id']), 'users_id'=>intval($users_id), 'is_deleted' => false])->first(); 
                    $row['liked_count'] = LikedPosts::where(['posts_id' => intval($row['id']), 'is_deleted' => false])->count();           
                    $row['comment_count'] = Comments::where(['posts_id' => intval($row['id']), 'is_deleted' => false])->count(); 
                    $row['shared_count'] = Posts::where(['posts_id' => intval($row['id']), 'is_deleted' => false])->count();           
                    
                    if (!empty($row['liked'])) {
                        $row['is_liked'] = $row['liked']['id'] === null ? 0 : 1;
                    } else {
                        $row['is_liked'] = 0; 
                    }
                
                    return $row;
                });

                $responseStatusCode = 200;
                $responseMessageId = 'HOME_DATA_SUCCESSFUL';
                $responseMessage = 'Home data successfully collected.';
                $responseData = ['user' => $user, 'users' => $users, 'posts' => $posts];
            } else {
                $responseStatusCode = 404;
                $responseMessageId = 'NOT_FOUND';
                $responseMessage = 'Data not found.';
                $responseData = []; 
            }
        } else {
            $responseStatusCode = 500;
            $responseMessageId = 'INTERNAL_SERVER_ERROR';
            $responseMessage = 'Internal server error.';
            $responseData = []; 
        }

        return response()->json($this->responseComponent->returnResponseDetails($responseStatusCode, $responseMessageId, $responseMessage, $responseData), $responseStatusCode);
    }
}
