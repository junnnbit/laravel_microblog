<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\Password;
use Laravel\Passport\TokenRepository;
use Laravel\Passport\RefreshTokenRepository;

use App\Models\Users;

use App\View\Components\ResponseComponent;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->responseComponent = new ResponseComponent();
    }

    public function index(){
        return response()->json(request()->user(), 200);
    }

    public function signup(Request $request) {
        $rules = [
            'firstname' => 'required|string|min:2|max:255|regex:/^[a-zA-Z0-9._ \-]+$/',
            'middlename' => 'nullable|string|min:2|max:255|regex:/^[a-zA-Z0-9._ \-]+$/',
            'lastname' => 'required|string|min:2|max:255|regex:/^[a-zA-Z0-9._ \-]+$/',
            'nickname' => 'nullable|string|min:2|max:255|regex:/^[a-zA-Z0-9._ \-]+$/',
            'gender' => 'required|string|min:4|max:255|regex:/^[a-zA-Z0-9._ \-]+$/|in:Male,Female,Others',
            'birthdate' => 'required|date|date_format:Y-m-d|before:-18 years',
            'lot_block_subdi' => 'required|string|max:255|regex:/^[a-zA-Z0-9._ \-]+$/',
            'street' => 'required|string|max:255|regex:/^[a-zA-Z0-9._ \-]+$/',
            'city' => 'required|string|max:255|regex:/^[a-zA-Z0-9._ \-]+$/',
            'province' => 'required|string|max:255|regex:/^[a-zA-Z0-9._ \-]+$/',
            'country' => 'required|string|max:255|regex:/^[a-zA-Z0-9._ \-]+$/',
            'zipcode' => 'required|string|min:4|max:10|regex:/^[0-9\-]+$/',
            'contact_num' => 'required|string|min:7|max:20|regex:/^([0-9\s\-\+\(\)]*)$/',
            'email' => 'required|string|email',
            'username' => 'required|string|min:6|max:255|unique:users|regex:/^[a-zA-Z0-9._]+$/',
            'password' => ['required', 'string', Password::min(8)->mixedCase()->numbers(), 'regex:/^[a-zA-Z0-9._ \-]+$/', 'confirmed']
        ];

        $customMessages = [
            'required' => '_REQUIRED:The :attribute field is required.',
            'string' => '_INVALID_TYPE:The :attribute must be a string.',
            'date' => '_INVALID_TYPE:The :attribute must be a date.',
            'regex' => '_INVALID_FORMAT:The :attribute format is invalid. Only accepts alpha-numeric characters, space and hyphen, underscore and period symbols.',
            'min' => '_MIN_COUNT:The :attribute field must be at least :min characters.',
            'max' => '_MAX_COUNT:The :attribute field must not be greater than :max characters.',
            'gender.in' => '_INVALID_VALUE:Please input Male, Female or Others only.',
            'birthdate.before' => '_MIN_AGE:Should be 18+ years of age to create an account.',
            'birthdate.date_format' => '_INVALID_FORMAT:Date should be in YYYY-MM-DD format.',
            'zipcode.regex' => '_INVALID_FORMAT:Allows only number and a hypen.',
            'contact_num.regex' => '_INVALID_FORMAT:Allows only number and a + or - symbol.',
            'email.email' => '_INVALID_FORMAT:The email must be a valid email address.',
            'unique' => '_TAKEN:The :attribute has already been taken.',
            'username.regex' => '_INVALID_FORMAT:The :attribute format is invalid. Only accepts alpha-numeric characters, underscore and period symbols.',
            'password.confirmed' => '_CONFIRM_PASSWORD_MISMATCH:The password confirmation does not match.'
        ];

        $validator = Validator::make($request->all(), $rules, $customMessages);

        if($validator->fails()){
            $error_messages = $validator->messages()->get('*');
            $errors = [];

            foreach ($error_messages as $field_name => $rules) {
                foreach ($rules as $rule_message) {
                    $message_id = explode(':', $rule_message)[0];
                    $message = explode(':', $rule_message)[1];
                    $errors[] = ['field_name' => $field_name, 'message_id' => strtoupper($field_name) . $message_id, 'message' => $message];
                }
            }

            $responseStatusCode = 400;
            $responseMessageId = 'INVALID_PARAMETERS';
            $responseMessage = 'Invalid Parameters.';
            $responseData = $errors;
        } else {
            $request['hash_code'] = md5(strval(rand(0,1000)) . $request['email']);

            if ($user = Users::create(array_merge($request->all(), ['password' => bcrypt($request->password)]))) {
                $responseStatusCode = 201;
                $responseMessageId = 'USER_CREATED';
                $responseMessage = 'New user successfully created.';
                $responseData = [];
            } else {
                $responseStatusCode = 500;
                $responseMessageId = 'INTERNAL_SERVER_ERROR';
                $responseMessage = 'Internal server error.';
                $responseData = []; 
            }
        }

        return response()->json($this->responseComponent->returnResponseDetails($responseStatusCode, $responseMessageId, $responseMessage, $responseData), $responseStatusCode);
    }

    public function login(Request $request) {

        $rules = [
            'username' => 'required|string|regex:/^[a-zA-Z0-9._]+$/',
            'password' => 'required|string|regex:/^[a-zA-Z0-9._ \-]+$/',
        ];

        $customMessages = [
            'required' => '_REQUIRED:The :attribute field is required.',
            'string' => '_INVALID_TYPE:The :attribute must be a string.',
            'regex' => '_INVALID_FORMAT:The :attribute format is invalid. Only accepts alpha-numeric characters, space and hyphen, underscore and period symbols.',
            'username.regex' => '_INVALID_FORMAT:The :attribute format is invalid. Only accepts alpha-numeric characters, underscore and period symbols.',
        ];

        $validator = Validator::make($request->all(), $rules, $customMessages);

        if ($validator->fails()) {
            $error_messages = $validator->messages()->get('*');
            $errors = [];

            foreach ($error_messages as $field_name => $rules) {
                foreach ($rules as $rule_message) {
                    $message_id = explode(':', $rule_message)[0];
                    $message = explode(':', $rule_message)[1];
                    $errors[] = ['field_name' => $field_name, 'message_id' => strtoupper($field_name) . $message_id, 'message' => $message];
                }
            }

            $responseStatusCode = 400;
            $responseMessageId = 'INVALID_PARAMETERS';
            $responseMessage = 'Invalid Parameters.';
            $responseData = $errors;
        } else {
            if (! auth()->attempt($validator->validated())) {
                $responseStatusCode = 409;
                $responseMessageId = 'USER_CREDENTIALS_NOT_FOUND';
                $responseMessage = 'Incorrect username or password.';
                $responseData = [];
            } else if (auth()->attempt($validator->validated())) {
                $user = auth()->user();

                if (! $user['is_verified'] || $user['status'] !== 'Active') {
                    $responseStatusCode = 401;
                    $responseMessageId = 'USER_NOT_VERIFIED';
                    $responseMessage = 'Please verify account first.';
                    $responseData = ['username' => $user['username']];
                } else {
                    $responseStatusCode = 200;
                    $responseMessageId = 'USER_LOGGED_IN';
                    $responseMessage = 'User successfully logged in.';
                    $accessToken = $user->createToken('authToken')->accessToken;
                    $responseData = ['username' => $user['username'], 'token' => $accessToken];
                }
            } else {
                $responseStatusCode = 500;
                $responseMessageId = 'INTERNAL_SERVER_ERROR';
                $responseMessage = 'Internal server error.';
                $responseData = []; 
            }
        }

        return response()->json($this->responseComponent->returnResponseDetails($responseStatusCode, $responseMessageId, $responseMessage, $responseData), $responseStatusCode);
    }

    public function logout(Request $request)
    {
        $user = auth()->user();
        if ($user->token()->revoke()) {
            $responseStatusCode = 200;
            $responseMessageId = 'USER_LOGGED_OUT';
            $responseMessage = 'User successfully logged out.';
            $responseData = ['username' => $user['username']];
        } else {
            $responseStatusCode = 500;
            $responseMessageId = 'INTERNAL_SERVER_ERROR';
            $responseMessage = 'Internal server error.';
            $responseData = []; 
        }

        return response()->json($this->responseComponent->returnResponseDetails($responseStatusCode, $responseMessageId, $responseMessage, $responseData), $responseStatusCode);
            
    }
}
