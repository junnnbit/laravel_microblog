<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use App\Http\Middleware\Authenticate as Middleware;

use App\Models\Users;

use Illuminate\Support\Facades\Route;

class ApiKey extends Middleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $defaultToken = $request->header('ApiKey');
        $userToken = $request->header('Authorization');

        $routeActions = Route::current()->gatherMiddleware();

        if (collect($routeActions)->contains('auth:api')) {
            if (!empty($userToken)) {
                $is_exists = Users::where('id' , Auth::guard('api')->id())->exists();
                if(!$is_exists){
                    return response()->json(['status_code' => 401, 'message_id' => 'UNAUTHORIZED_ACCESS', 'message' => 'Unauthorized access. Invalid token.'], 401);
                }
            } else {
                return response()->json(['status_code' => 401, 'message_id' => 'UNAUTHORIZED_ACCESS', 'message' => 'Unauthorized access. Token not found.'], 401);
            }
        } else {
            if (!empty($defaultToken)) {
                $registeredToken = 'TGFyYXZlbDhNaWNyb2Jsb2dBcGlBY2Nlc3NLZXk=';
                if ($defaultToken !== $registeredToken) {
                    return response()->json(['status_code' => 401, 'message_id' => 'UNAUTHORIZED_ACCESS', 'message' => 'Unauthorized access. Invalid token.'], 401);
                }
            } else {
                return response()->json(['status_code' => 401, 'message_id' => 'UNAUTHORIZED_ACCESS', 'message' => 'Unauthorized access. Token not found.'], 401);
            } 
        } 
        
        return $next($request);
    }
}
