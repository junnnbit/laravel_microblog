<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var string[]
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var string[]
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->renderable(function (Throwable $e, $request) {
            if ($request->is('api/*')) {
                if ($e->getStatusCode() === 404) {
                    return response()->json(['status_code' => 404, 'message_id' => 'PAGE_NOT_FOUND', 'message'=>'Page not found.'], 404);
                }

                if ($e->getStatusCode() === 500) {
                    return response()->json(['status_code' => 500, 'message_id' => 'INTERNAL_SERVER_ERROR', 'message'=>'Internal Server Error'], 500);
                }
            }
        });

        $this->reportable(function (Throwable $e) {
            //
        });
    }
}
