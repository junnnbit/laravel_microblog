<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FollowersFollowing extends Model
{
    use HasFactory;

    protected $table = 'followers_following';
    public $timestamps = false;
}
